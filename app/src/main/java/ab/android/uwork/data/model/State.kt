package ab.android.uwork.data.model

enum class State {
    DONE, LOADING, ERROR
}