package ab.android.uwork.data.repo

import ab.android.uwork.data.api.ApiService
import ab.android.uwork.data.model.WeatherResponse
import io.reactivex.Single
import javax.inject.Inject

class WeatherRepository @Inject constructor(
    private val api: ApiService
) {

    fun getWeather(id: String, lang: String, units: String): Single<WeatherResponse> {
        return api.getWeather(id, lang, units)
    }
}