package ab.android.uwork.data.datasource

import ab.android.uwork.data.api.ApiService
import ab.android.uwork.data.model.PicPhotos
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import io.reactivex.disposables.CompositeDisposable

class ImagesDataSourceFactory(
    private val compositeDisposable: CompositeDisposable,
    private val api: ApiService)
    : DataSource.Factory<Int, PicPhotos>() {

    val newsDataSourceLiveData = MutableLiveData<ImagesDataSource>()

    override fun create(): DataSource<Int, PicPhotos> {
        val newsDataSource = ImagesDataSource(api, compositeDisposable)
        newsDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }
}