package ab.android.uwork.data.datasource

import ab.android.uwork.data.api.ApiService
import ab.android.uwork.data.model.PicPhotos
import ab.android.uwork.data.model.State
import ab.android.uwork.utils.AppConstants
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers

class ImagesDataSource(
        private val api: ApiService,
        private val compositeDisposable: CompositeDisposable
)
    : PageKeyedDataSource<Int, PicPhotos>() {

    var state: MutableLiveData<State> = MutableLiveData()
    private var retryCompletable: Completable? = null


    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, PicPhotos>) {
        updateState(State.LOADING)
        compositeDisposable.add(
                api.getPhotos(1, AppConstants.LIMIT)
                        .subscribe(
                                { response ->
                                    updateState(State.DONE)
                                    callback.onResult(response,
                                            null,
                                            2
                                    )
                                },
                                {
                                    updateState(State.ERROR)
                                    setRetry(Action { loadInitial(params, callback) })
                                }
                        )
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, PicPhotos>) {
        updateState(State.LOADING)
        compositeDisposable.add(
                api.getPhotos(params.key, AppConstants.LIMIT)
                        .subscribe(
                                { response ->
                                    updateState(State.DONE)
                                    callback.onResult(response,
                                            params.key + 1
                                    )
                                },
                                {
                                    updateState(State.ERROR)
                                    setRetry(Action { loadAfter(params, callback) })
                                }
                        )
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, PicPhotos>) {
    }

    private fun updateState(state: State) {
        this.state.postValue(state)
    }

    fun retry() {
        if (retryCompletable != null) {
            compositeDisposable.add(retryCompletable!!
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe())
        }
    }

    private fun setRetry(action: Action?) {
        retryCompletable = if (action == null) null else Completable.fromAction(action)
    }

}