package ab.android.uwork.data.api

import ab.android.uwork.data.model.PicPhotos
import ab.android.uwork.data.model.WeatherResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("weather")
    fun getWeather(
            @Query("id") id: String,
            @Query("lang") lang: String,
            @Query("units") units: String
    ): Single<WeatherResponse>

    @GET("list")
    fun getPhotos(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Single<List<PicPhotos>>
}