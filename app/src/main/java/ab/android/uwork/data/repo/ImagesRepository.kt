package ab.android.uwork.data.repo

import ab.android.uwork.data.api.ApiService
import ab.android.uwork.data.datasource.ImagesDataSource
import ab.android.uwork.data.datasource.ImagesDataSourceFactory
import ab.android.uwork.data.model.PicPhotos
import ab.android.uwork.data.model.State
import ab.android.uwork.utils.AppConstants
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ImagesRepository @Inject constructor(
    api: ApiService
) {

    private val compositeDisposable = CompositeDisposable()
    private val imagesDataSourceFactory: ImagesDataSourceFactory =
        ImagesDataSourceFactory(compositeDisposable, api)

    fun getPhotos(): LiveData<PagedList<PicPhotos>> {
        val config = PagedList.Config.Builder()
            .setPageSize(AppConstants.PAGE_SIZE)
            .setEnablePlaceholders(false)
            .build()
        return LivePagedListBuilder(imagesDataSourceFactory, config).build()
    }

    fun getState(): LiveData<State> =
        Transformations.switchMap(
            imagesDataSourceFactory.newsDataSourceLiveData,
            ImagesDataSource::state
        )

    fun clear() {
        compositeDisposable.clear()
    }

}