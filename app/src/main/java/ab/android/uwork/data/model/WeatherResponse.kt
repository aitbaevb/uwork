package ab.android.uwork.data.model

import ab.android.uwork.R

data class WeatherResponse(
    val name: String,
    val main: WeatherMain,
    val clouds: Cloud
) {
    override fun toString(): String {
        return String.format(
                "%s, Температура: %.1f, Облачность: %.1f, Влажность: %.1f",
                name,
                main.temp,
                clouds.all,
                main.humidity)
    }
}

data class WeatherMain(
    val temp: Float,
    val humidity: Float
)

data class Cloud(
    val all: Float
)