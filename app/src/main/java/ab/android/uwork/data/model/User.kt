package ab.android.uwork.data.model

import ab.android.uwork.utils.AppConstants
import android.util.Patterns

data class UserLogin(
        val email: String?,
        val password: String?
) {

    fun isValidEmail(): Boolean {
        return !email.isNullOrEmpty() &&
                Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun isValidPassword(): Boolean {
        return !password.isNullOrEmpty() &&
                password.matches(AppConstants.PASSWORD_PATTERN.toRegex())
    }

}