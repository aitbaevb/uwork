package ab.android.uwork.data.model

data class PicPhotos(
    val id: Int,
    val author: String,
    val download_url: String
)