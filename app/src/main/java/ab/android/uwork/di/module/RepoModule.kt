package ab.android.uwork.di.module

import ab.android.uwork.data.api.ApiService
import ab.android.uwork.data.repo.ImagesRepository
import ab.android.uwork.data.repo.WeatherRepository
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton


@Module
class RepoModule {

    @Provides
    @Singleton
    fun provideImagesRepo(@Named("picsum") api: ApiService): ImagesRepository = ImagesRepository(api)

    @Provides
    @Singleton
    fun provideWeatherRepo(@Named("weather") api: ApiService): WeatherRepository = WeatherRepository(api)
}

