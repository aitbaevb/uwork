package ab.android.uwork.di

import ab.android.uwork.App
import ab.android.uwork.di.module.ViewModelsModule
import dagger.Component
import ab.android.uwork.di.module.ApplicationModule
import ab.android.uwork.di.module.NetworkModule
import ab.android.uwork.di.module.RepoModule
import ab.android.uwork.ui.auth.SignInFragment
import ab.android.uwork.ui.images.ImagesFragment
import ab.android.uwork.ui.main.MainActivity
import javax.inject.Singleton

@Component(modules = [
    RepoModule::class,
    ApplicationModule::class,
    NetworkModule::class,
    ViewModelsModule::class])
@Singleton
interface ApplicationComponent {

    // application
    fun inject(application: App)

    fun inject(mainActivity: MainActivity)
    fun inject(signInFragment: SignInFragment)
    fun inject(imagesFragment: ImagesFragment)
}