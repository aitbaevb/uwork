package ab.android.uwork.di.module

import ab.android.uwork.data.api.ApiService
import ab.android.uwork.utils.AppConstants
import dagger.Module
import dagger.Provides
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
class NetworkModule {

    @Provides
    @Singleton
    @Named("picsum")
    fun providePicsumOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .addInterceptor { chain ->
            val request: Request = chain.request().newBuilder().build()
            chain.proceed(request)
        }.build()
    }

    @Provides
    @Singleton
    @Named("picsum")
    fun providePicsumRetrofit(@Named("picsum") okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(AppConstants.PICSUM_URL)
                .build()
    }

    @Provides
    @Singleton
    @Named("weather")
    fun provideWeatherOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
            .addInterceptor { chain ->
                var request: Request = chain.request()
                val url: HttpUrl = request.url
                    .newBuilder()
                    .addQueryParameter("appid", AppConstants.APP_ID)
                    .build()
                request = request.newBuilder().url(url).build()
                chain.proceed(request)
            }.build()
    }

    @Provides
    @Singleton
    @Named("weather")
    fun provideWeatherRetrofit(@Named("weather") okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(AppConstants.WEATHER_URL)
            .build()
    }

    @Provides
    @Singleton
    @Named("picsum")
    fun providePicsumApiService(@Named("picsum") retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @Singleton
    @Named("weather")
    fun provideWeatherApiService(@Named("weather") retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun providesGsonConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    fun providesRxJavaCallAdapterFactory(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.create()
    }

}

