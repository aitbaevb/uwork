package ab.android.uwork.utils

class AppConstants {
    companion object {

        // picsum api params
        const val PAGE_SIZE = 10
        const val LIMIT = 10
        const val PICSUM_URL = "https://picsum.photos/v2/"

        // open weather api params
        const val WEATHER_URL = "https://api.openweathermap.org/data/2.5/"
        const val APP_ID = "c35880b49ff95391b3a6d0edd0c722eb"
        const val CITY_ID = "498817"
        const val LANG = "ru"
        const val UNITS = "metric"

        const val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$"
    }
}