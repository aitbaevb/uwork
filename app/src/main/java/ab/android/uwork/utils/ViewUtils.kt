package ab.android.uwork.utils

import ab.android.uwork.R
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import com.google.android.material.snackbar.Snackbar

fun View.snackbar(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}

fun Context.progress(): Dialog {
    val progressDialog = Dialog(this)
    progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    progressDialog.setCancelable(false)
    progressDialog.setContentView(R.layout.progress_layout)
    progressDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
    return progressDialog
}

fun Activity.closeKeyboard() {
    val view = this.currentFocus
    view?.let { v ->
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(v.windowToken, 0)
    }
}