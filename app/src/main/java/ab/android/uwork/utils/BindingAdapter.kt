package ab.android.uwork.utils

import ab.android.uwork.R
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso


class BindingAdapter {
    companion object {

        @BindingAdapter(value = ["android:image"])
        @JvmStatic
        fun setIcon(imageView: ImageView, imageUrl: String?) {
            imageUrl?.let {
                Picasso.get().load(it)
                    .resize(720, 640)
                    .centerInside()
                    .placeholder(R.drawable.logo)
                    .into(imageView)
            }
        }
    }
}