package ab.android.uwork.ui.auth

import ab.android.uwork.data.model.WeatherResponse
import ab.android.uwork.data.repo.WeatherRepository
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SignInViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository
): ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    private val _weather = MutableLiveData<WeatherResponse>()
    val weather: LiveData<WeatherResponse> get() = _weather

    private val _error = MutableLiveData<Throwable>()
    val error: LiveData<Throwable> get() = _error

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> get() = _loading

    fun getWeather(id: String, lang: String, units: String) {
        _loading.value = true
        compositeDisposable.add(
            weatherRepository.getWeather(id, lang, units)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _loading.postValue(false)
                    _weather.postValue(it)
                }, {
                    _loading.postValue(false)
                    _error.postValue(it)
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}