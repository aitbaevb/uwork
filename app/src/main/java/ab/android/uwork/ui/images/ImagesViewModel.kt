package ab.android.uwork.ui.images

import ab.android.uwork.data.model.PicPhotos
import ab.android.uwork.data.model.State
import ab.android.uwork.data.repo.ImagesRepository
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import javax.inject.Inject

class ImagesViewModel @Inject constructor(
    private val imagesRepository: ImagesRepository
): ViewModel() {

    var imageList: LiveData<PagedList<PicPhotos>> = imagesRepository.getPhotos()

    override fun onCleared() {
        super.onCleared()
        imagesRepository.clear()
    }

    fun getState(): LiveData<State> = imagesRepository.getState()

    fun listIsEmpty(): Boolean {
        return imageList.value?.isEmpty() ?: true
    }

}