package ab.android.uwork.ui.auth

import ab.android.uwork.App
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ab.android.uwork.R
import ab.android.uwork.data.model.UserLogin
import ab.android.uwork.databinding.FragmentSignInBinding
import ab.android.uwork.utils.AppConstants
import ab.android.uwork.utils.closeKeyboard
import ab.android.uwork.utils.progress
import ab.android.uwork.utils.snackbar
import android.app.Dialog
import android.content.Context
import android.util.Patterns
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import javax.inject.Inject

class SignInFragment : Fragment() {

    private lateinit var binding: FragmentSignInBinding
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: SignInViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(SignInViewModel::class.java)
    }
    private lateinit var progress: Dialog

    override fun onAttach(context: Context) {
        super.onAttach(context)
        progress = context.progress()
        (context.applicationContext as App).component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false)

        setOnClickListeners()
        observeViewModel()
        return binding.root
    }

    private fun setOnClickListeners() {
        binding.signInBtn.setOnClickListener {
            validateCredentials()
        }
    }

    private fun validateCredentials() {
        val userLogin = UserLogin(
                binding.loginEdit.text.toString(),
                binding.passwordEdit.text.toString()
        )
        if (!userLogin.isValidEmail()) {
            requireView().snackbar(getString(R.string.wrong_email_msg))
            return
        }
        if (!userLogin.isValidPassword()) {
            requireView().snackbar(getString(R.string.wrong_password_msg))
            return
        }
        viewModel.getWeather(AppConstants.CITY_ID, AppConstants.LANG, AppConstants.UNITS)
    }

    private fun observeViewModel() {
        viewModel.error.observe(viewLifecycleOwner, {
            requireView().snackbar(it.message ?: "")
        })

        viewModel.loading.observe(viewLifecycleOwner, {
            activity?.closeKeyboard()
            if (it) progress.show() else progress.dismiss()
        })

        viewModel.weather.observe(viewLifecycleOwner, {
            requireView().snackbar(it.toString())
        })
    }
}