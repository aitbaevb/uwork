package ab.android.uwork.ui.images

import ab.android.uwork.R
import ab.android.uwork.data.model.PicPhotos
import ab.android.uwork.data.model.State
import ab.android.uwork.databinding.ItemImageFooterBinding
import ab.android.uwork.databinding.ItemImagesBinding
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class ImagesAdapter: PagedListAdapter<PicPhotos, RecyclerView.ViewHolder>(DiffCallback) {

    private var state = State.LOADING

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val imagesHolder = ImagesViewHolder.create(parent)
        val footerHolder = ListFooterViewHolder.create(parent)

        return if (viewType == DATA_VIEW_TYPE) imagesHolder else footerHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == DATA_VIEW_TYPE)
            (holder as ImagesViewHolder).bind(getItem(position))
        else (holder as ListFooterViewHolder).bind(state)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) DATA_VIEW_TYPE else FOOTER_VIEW_TYPE
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == State.LOADING || state == State.ERROR)
    }

    class ListFooterViewHolder(private val binding: ItemImageFooterBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(status: State?) {
            binding.state = status
            binding.executePendingBindings()
        }

        companion object {
            fun create(parent: ViewGroup): ListFooterViewHolder {
                val binding = DataBindingUtil.inflate<ItemImageFooterBinding>(
                    LayoutInflater.from(parent.context), R.layout.item_image_footer, parent, false
                )
                return ListFooterViewHolder(binding)
            }
        }
    }

    class ImagesViewHolder(private val binding: ItemImagesBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: PicPhotos?) {
            if (item != null) {
                binding.image = item
                binding.executePendingBindings()
            }
        }

        companion object {
            fun create(parent: ViewGroup): ImagesViewHolder {
                val binding = DataBindingUtil.inflate<ItemImagesBinding>(
                    LayoutInflater.from(parent.context), R.layout.item_images, parent, false
                )
                return ImagesViewHolder(binding)
            }
        }
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }


    companion object {

        const val DATA_VIEW_TYPE = 1
        const val FOOTER_VIEW_TYPE = 2

        val DiffCallback = object : DiffUtil.ItemCallback<PicPhotos>() {
            override fun areItemsTheSame(oldItem: PicPhotos, newItem: PicPhotos): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: PicPhotos, newItem: PicPhotos): Boolean {
                return oldItem == newItem
            }
        }
    }
}