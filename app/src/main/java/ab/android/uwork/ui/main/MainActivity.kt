package ab.android.uwork.ui.main

import ab.android.uwork.R
import ab.android.uwork.databinding.ActivityMainBinding
import ab.android.uwork.ui.auth.SignInFragment
import ab.android.uwork.ui.images.ImagesFragment
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var activeFragment = Fragment()
    private var imagesFragment = ImagesFragment()
    private var signInFragment = SignInFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setUpBottomNav()
        initFragments()
    }

    private fun initFragments() {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.main_frame, signInFragment)
            .hide(signInFragment)
            .commit()
        supportFragmentManager
            .beginTransaction()
            .add(R.id.main_frame, imagesFragment)
            .commit()
        activeFragment = imagesFragment
    }

    private fun setUpBottomNav() {
        binding.bottomnav.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.nav_cartoons -> {
                    supportFragmentManager
                        .beginTransaction()
                        .hide(activeFragment)
                        .show(imagesFragment)
                        .commit()
                    activeFragment = imagesFragment
                }
                R.id.nav_signin -> {
                    supportFragmentManager
                        .beginTransaction()
                        .hide(activeFragment)
                        .show(signInFragment)
                        .commit()
                    activeFragment = signInFragment
                }
            }
            true
        }
    }
}