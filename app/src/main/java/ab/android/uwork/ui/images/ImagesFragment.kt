package ab.android.uwork.ui.images

import ab.android.uwork.App
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ab.android.uwork.R
import ab.android.uwork.data.model.State
import ab.android.uwork.databinding.FragmentImagesBinding
import android.content.Context
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import javax.inject.Inject

class ImagesFragment : Fragment() {

    private lateinit var binding: FragmentImagesBinding
    private lateinit var adapter: ImagesAdapter
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: ImagesViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ImagesViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as App).component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_images, container, false)

        initState()
        initImagesAdapter()
        observeViewModel()
        return binding.root
    }

    private fun initState() {
        viewModel.getState().observe(viewLifecycleOwner, { state ->
            binding.imagesProgress.visibility =
                if (viewModel.listIsEmpty() && state == State.LOADING)
                    View.VISIBLE
                else
                    View.GONE
            if (!viewModel.listIsEmpty()) {
                adapter.setState(state ?: State.DONE)
            }
        })
    }

    private fun initImagesAdapter() {
        adapter = ImagesAdapter()
        binding.imagesRecyclerview.adapter = adapter
    }

    private fun observeViewModel() {
        viewModel.imageList.observe(viewLifecycleOwner, {
            adapter.submitList(it)
        })
    }

}